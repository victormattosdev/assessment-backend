# Tecnologias Utilizadas

Foi utilizado o XAMPP v7.3.11 (PHP v7.3.11, MariaDB 10.4.8, phpMyAdmin 4.9.1). 

Para o modal de delete foi usado o SweetAlert 2.

## Importador Produtos CLI

Com o banco mysql devidamente criado e o xampp instalado, acesse o bin do mysql "C:\xampp\mysql\bin" e digite o comando

```bash
mysql.exe -u root
```

Após conectado na interface, digite

```bash
load data local infile 'C:/path/to/destination/import.csv' 
into table products fields terminated by ';' lines terminated by '\n' 
ignore 1 rows (name, sku, description, quantity, price, category);
```
