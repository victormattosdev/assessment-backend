<?php
include_once '../config/database.php';

class Product
{ 
    private $conn;
    private $table_name = "products";

    public $id;
    public $name;
    public $sku;
    public $price;
    public $description;
    public $quantity;
    public $image;
 
    public function __construct()
    {
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    function read()
    { 
        $query = "SELECT
                    id, name, sku, price, description, quantity, category
                FROM
                    " . $this->table_name . "
                WHERE
                    id = ?
                LIMIT
                    0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->name = $row['name'];
        $this->sku = $row['sku'];
        $this->price = $row['price'];
        $this->description = $row['description'];
        $this->quantity = $row['quantity'];
        $this->category = $row['category'];
    }

    function create()
    { 
        $query = "INSERT INTO " . $this->table_name . "
        SET
        name=:name, sku=:sku, price=:price, description=:description, quantity=:quantity, category=:category, image=:image";
        
        $stmt = $this->conn->prepare($query);
        
        //Valores do form
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->sku=htmlspecialchars(strip_tags($this->sku));
        $this->price=htmlspecialchars(strip_tags($this->price));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->quantity=htmlspecialchars(strip_tags($this->quantity));
        $this->category=htmlspecialchars(strip_tags($this->category));
        $this->image=htmlspecialchars(strip_tags($this->image));
        
        //Setando valores na query 
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":quantity", $this->quantity);
        $stmt->bindParam(":category", $this->category);
        $stmt->bindParam(":image", $this->image);
        
        if($stmt->execute()){
            $this->uploadPhoto();

            return true;
        }else{
            return false;
        }
        
    }
    
    function update()
    {
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    name = :name,
                    sku = :sku,
                    price = :price,
                    description = :description,
                    quantity = :quantity,
                    category  = :category
                WHERE
                    id = :id";
     
        $stmt = $this->conn->prepare($query);
     
        //Valores do form
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->sku=htmlspecialchars(strip_tags($this->sku));
        $this->price=htmlspecialchars(strip_tags($this->price));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->quantity=htmlspecialchars(strip_tags($this->quantity));
        $this->category=htmlspecialchars(strip_tags($this->category));
 
        //Setando valores na query 
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":quantity", $this->quantity);
        $stmt->bindParam(":category", $this->category);
     
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }
    
    function delete()
    {
 
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
         
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
     
        if($result = $stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

    function paginate()
    { 
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        
        $perPage    = 10;
        $fromRecord = ($perPage * $page) - $perPage;

        $query = "SELECT
                    id, name, sku, price, description, quantity, category, image
                FROM
                    " . $this->table_name . "
                ORDER BY
                    name ASC
                LIMIT
                    {$fromRecord}, {$perPage}";
     
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
     
        return $data = [
            'products' => $stmt,
            'page'    => $page,
            'perPage' => $perPage
        ];
    }

    function countAll()
    {
        $query = "SELECT id FROM " . $this->table_name . "";
 
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
    
        $num = $stmt->rowCount();
    
        return $num;
    }


    function uploadPhoto()
    {    
        if($this->image){
            $directory = "../assets/images/product/";
            $file = $directory . $this->image;
 
            move_uploaded_file($_FILES["image"]["tmp_name"], $file);
        }
    }

}
?>