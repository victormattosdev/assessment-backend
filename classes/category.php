<?php
include_once '../config/database.php';

class Category
{
    private $conn;
    private $table_name = "categories";

    public $id;
    public $name;
    public $code;
 
    public function __construct()
    {
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    function list()
    { 
        $query = "SELECT
                    id, name
                FROM
                    " . $this->table_name . "
                ORDER BY
                    name ASC";
     
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();
     
        return $result;
    }

    function read()
    {
        $query = "SELECT
                    id, name, code
                FROM
                    " . $this->table_name . "
                WHERE
                    id = ?
                LIMIT
                    0,1";
     
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
     
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
     
        $this->name = $row['name'];
        $this->code = $row['code'];
    }

    function create()
    { 
        $query = "INSERT INTO " . $this->table_name . "
                SET
                    name=:name, code=:code";

        $stmt = $this->conn->prepare($query);

        //Valores do form
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->sku=htmlspecialchars(strip_tags($this->code));
 
        //Setando valores na query 
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":code", $this->code);
 
        if($stmt->execute()){
            return true;
        } else {
            return false;
        }
 
    }

    function update()
    {
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    name = :name,
                    code = :code
                WHERE
                    id = :id";
     
        $stmt = $this->conn->prepare($query);
     
        //Valores do form
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->code=htmlspecialchars(strip_tags($this->code ));
 
        //Setando valores na query 
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":code", $this->code);
     
        // execute the query
        if($stmt->execute()){
            return true;
        }
     
        return false;
         
    }

    function delete()
    {
 
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
         
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
     
        if($result = $stmt->execute()){
            return true;
        }else{
            return false;
        }
    } 

    function paginate()
    { 
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
 
        $perPage    = 10;
        $fromRecord = ($perPage * $page) - $perPage;

        $query = "SELECT
                    id, name, code
                FROM
                    " . $this->table_name . "
                ORDER BY
                    name ASC
                LIMIT
                    {$fromRecord}, {$perPage}";
     
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
     
        return $data = [
            'categories' => $stmt,
            'page'       => $page,
            'perPage'    => $perPage
        ];
    }

    function countAll()
    {
        $query = "SELECT id FROM " . $this->table_name . "";
 
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
    
        $num = $stmt->rowCount();
    
        return $num;
    }
 
}
?>