<?php
    $page = $data['page'];

    echo "<ul class='pagination'>";
    
    if ($page > 1) {
        echo "<li><a href='?page=1' title='Go to the first page.'>";
            echo "First";
        echo "</a></li>";
    } 

    $range      = 2;
    $totalPages = ceil($rows / $data['perPage']);
    
    $initial = $page - $range;
    $limit = ($page + $range)  + 1;
    
    for ($x = $initial; $x < $limit; $x++) {
    
        if (($x > 0) && ($x <= $totalPages)) {
            if ($x == $page) {
                echo "<li><a  class='active' href=\"#\">" . $x . "</a></li>";
            } 
    
            else {
                echo "<li><a href='?page=" . $x . "'>" . $x . "</a></li>";
            }
        }
    }
    
    if($page<$totalPages){
        echo "<li><a href='?page=" . $totalPages . "' title='Last page is " . $totalPages . "'>";
            echo "Last";
        echo "</a></li>";
    }
    
    echo "</ul>";
?>